# 06.11.2019 22:48
## _Сomments and suggestions to the [website](https://stiltsoft.com/index.html)_ 
- The main page isn't optimized for mobile phone screens; All modern web-sites are customized to mobile screens. It would be more convinient for visitors if your web-site was fully optimized for mobile phone screens;
- A big design mismatch between the page [Knowledge hub](https://knowledge.stiltsoft.com/?utm_campaign=knowledge-hub&utm_medium=site&utm_source=stiltsoft-site-button) and the rest of the website. 
  
- The design itself is quite outdated. The page [Knowledge hub](https://knowledge.stiltsoft.com/?utm_campaign=knowledge-hub&utm_medium=site&utm_source=stiltsoft-site-button) looks more mordern and eye-catching;
- The design of the blog page sticks out from the other pages;
- Stiltsoft is a company that provides software to more than 80 contries all over the world and there's only English on the website; 
- On the main page there's a section with your partners (intel, netflix and etc). If to follow any of these partners links the website takes you to the [about page](https://stiltsoft.com/about.html);
- I understand that it's not really important but on any page with documentation [like here](https://docs.stiltsoft.com/display/public/TableFilter/Table+Filter+for+Confluence) you may choose both options of **what do you think about our documantation?** at the same time;
- Perhaps it's your company's policy, but your website doesn't have a **career** page;
- On the main page there was mentioned that you make apps for Jira, Confluence and **Bitbucket**. But if you click on [Check all apps](https://stiltsoft.com/apps.html) there are only options **most popular**, **for confluence**, **for Jira** and there's no separate option for **bitbucket** (only in **the most popular**); 
- If you're on the [blog page](https://stiltsoft.com/blog/) and scroll down to the end, there is a link to **most popular add-ons**. And if'you follow this link you'll find yourself on the page with **404 - Page you requested not found**;
- **The services** button on this [page](https://stiltsoft.com/sponsored-development.html) doesn't work. 

#### P.S 06.11.2019 22:49